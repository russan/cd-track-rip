#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <tclap/CmdLine.h>
#include <sys/stat.h>

extern "C" {
#include <cdio/cdda.h>
#include <cdio/paranoia.h>
}

using namespace std;

int main(int argc, const char *argv[]) {
    // First, parse arguments
    string dev;
    string path;
    vector<int> track_list;
    bool list;
    bool eject;
    int retry;

    try {
        TCLAP::CmdLine cmd("cd-track-rip", ' ', "0.9 beta");

        TCLAP::ValueArg<string> deviceArg("d", "device", "Source CD device (e.g.: /dev/cdrom, E:)", false, "", "path");
        cmd.add(deviceArg);

        TCLAP::MultiArg<int> tracksArg("t", "track",
                                       "Track or tracks to rip. Will rip all tracks if not specified. You may list as many tracks as you like.",
                                       false, "#");
        cmd.add(tracksArg);

        TCLAP::ValueArg<string> pathArg("o", "output-dir",
                                        "WAV files will be created in this directory. File names are like \"track<number>.wav\". Path will be created if it doesn't exist. Files of the same name will be overwritten.",
                                        false, "./", "path");
        cmd.add(pathArg);

        TCLAP::ValueArg<int> retryArg("r", "retry", "Number of times to retry a failed sector.", false, 5, "#");
        cmd.add(retryArg);

        TCLAP::SwitchArg listArg("l", "list", "Just list the tracks on the CD. Don't rip anything.", cmd, false);

        TCLAP::SwitchArg ejectArg("e", "eject", "Eject the CD when complete.", cmd, false);

        cmd.parse(argc, argv);

        dev = deviceArg.getValue();
        track_list = tracksArg.getValue();
        list = listArg.getValue();
        retry = retryArg.getValue();
        eject = ejectArg.getValue();

        char c_path[256];
        realpath(pathArg.getValue().c_str(), c_path);
        path = string(c_path);
    }
    catch (TCLAP::ArgException &e) {
        cerr << e.error() << " for arg " << e.argId() << endl;
        return -1;
    }

    cdrom_drive_t *d;
    if (dev.length() > 0) {
        d = cdio_cddap_identify(dev.c_str(), CDDA_MESSAGE_FORGETIT, NULL);
    }
    else {
        d = cdio_cddap_find_a_cdrom(CDDA_MESSAGE_FORGETIT, NULL);
    }

    if (d == NULL) {
        cerr << "Unable to find an audio CD." << endl;
        return -1;
    }

    if (cdio_cddap_open(d) != 0) {
        cerr << "Unable to open disc." << endl;
        return -1;
    }

    cout << "device=" << d->cdda_device_name << endl;

    if (list) {
        track_list.clear();
        TOC_t *toc = d->disc_toc;
        for (int i = 0; i < d->tracks; ++i) {
            cout << "track=" << (int) toc[i].bTrack << " lsn=" << toc[i].dwStartSector;

            lsn_t start = cdio_get_track_lsn(d->p_cdio, toc[i].bTrack);
            lsn_t end = cdio_get_track_last_lsn(d->p_cdio, toc[i].bTrack);
            unsigned int len = (unsigned int) (end - start + 1);

            cout << " duration=" << len / 75 / 60 << ":" << setfill('0') << setw(2) << len / 75 % 60 << "." <<
            (len % 75) * 100 / 75 << setw(0) << endl;
        }
    }
    else {
        cdrom_paranoia_t *p = cdio_paranoia_init(d);
        cdio_paranoia_modeset(p, PARANOIA_MODE_FULL ^ PARANOIA_MODE_NEVERSKIP);

        track_t tracks = cdio_cddap_tracks(d);
        for (int i = 1; i <= tracks; ++i) {
            if (track_list.size() > 0 && find(track_list.begin(), track_list.end(), i) >= track_list.end()) {
                cerr << "track=" << i << " skip" << endl;
                continue;
            }

            lsn_t first_lsn = cdio_cddap_track_firstsector(d, (track_t) i);
            lsn_t last_lsn = cdio_cddap_track_lastsector(d, (track_t) i);
            unsigned int len = (unsigned int) (last_lsn - first_lsn + 1);

            mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            char buf[256];
            snprintf(buf, 256, "%s/track%i.wav", path.c_str(), i);
            ofstream outfile(buf, ofstream::binary | ofstream::trunc | ofstream::out);

            cout << "track=" << i << " lsn=" << first_lsn;
            cout << " duration=" << len / 75 / 60 << ":" << setfill('0') << setw(2) << len / 75 % 60 << "." <<
            (len % 75) * 100 / 75 << setw(0);
            cout << " path=" << buf << endl;

            cdio_paranoia_seek(p, first_lsn, SEEK_SET);
            unsigned int byte_count = (unsigned int) ((last_lsn - first_lsn + 1) * CDIO_CD_FRAMESIZE_RAW);

            // WAV file header
            const int waveChunkLength = byte_count + 44 - 8;
            const int fmtChunkLength = 16;
            const int compressionCode = 1;
            const int numberOfChannels = cdio_cddap_track_channels(d, (track_t) i);
            const int sampleRate = 44100;  // Hz
            const int blockAlign = sampleRate * 2 * 2;
            const int significantBps = 4;
            const int extraFormatBytes = 16;

            // RIFF header
            outfile.write("RIFF", 4);
            outfile.write((char *) &waveChunkLength, 4);
            outfile.write("WAVE", 4);

            // FMT chunk
            outfile.write("fmt ", 4);
            outfile.write((char *) &fmtChunkLength, 4);
            outfile.write((char *) &compressionCode, 2);
            outfile.write((char *) &numberOfChannels, 2);
            outfile.write((char *) &sampleRate, 4);
            outfile.write((char *) &blockAlign, 4);
            outfile.write((char *) &significantBps, 2);
            outfile.write((char *) &extraFormatBytes, 2);

            // DATA chunk
            outfile.write("data", 4);
            outfile.write((char *) &byte_count, 4);

            int percent = 0;

            // Read data into the WAV file as fast as the CD will let us
            for (lsn_t i_cursor = first_lsn; i_cursor <= last_lsn; ++i_cursor) {
                int16_t *p_buf = cdio_paranoia_read_limited(p, NULL, retry);

                char *psz_err = cdio_cddap_errors(d);
                char *psz_mes = cdio_cddap_messages(d);

                if (psz_mes || psz_err)
                    cerr << psz_err << psz_mes;

                if (psz_err) {
                    free(psz_err);
                }
                if (psz_mes) {
                    free(psz_mes);
                }

                if (!p_buf) {
                    cerr << "Read error. Disc might be scratched. Exiting." << endl;
                    break;
                }

                char *temp = (char *) p_buf;
                outfile.write(temp, CDIO_CD_FRAMESIZE_RAW);

                int new_percent = (i_cursor - first_lsn) * 100 / (last_lsn - first_lsn);
                if (new_percent != percent) {
                    cout << "progress=" << new_percent << endl;
                    percent = new_percent;
                }
            }

            outfile.close();
        }

        cdio_paranoia_free(p);
    }

    if (eject) {
        cdio_eject_media_drive(d->cdda_device_name);
    }

    cdio_cddap_close(d);
    exit(0);
}